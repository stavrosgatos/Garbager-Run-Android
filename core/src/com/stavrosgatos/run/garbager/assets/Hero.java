package com.stavrosgatos.run.garbager.assets;

import com.badlogic.gdx.graphics.Texture;
import com.stavrosgatos.run.garbager.common.constants.Constants;
import com.stavrosgatos.run.garbager.common.utils.Utils;

public class Hero {

    private Texture[] hero;
    private int[] positionX;
    private int positionY;
    private boolean jumping;

    public Hero() {
        Utils utils = new Utils();
        this.hero = new Texture[Constants.HERO_STATES];
        this.positionX = new int[Constants.HERO_STATES];
        for (int i = 0; i < Constants.HERO_STATES; i++) {
            this.hero[i] = new Texture(Constants.HERO_SPRITE + (i + 1) + Constants.HERO_SPRITE_EXTENSION);
            this.positionX[i] = utils.getCenterPositionX(hero[i]);
        }
    }

    public Texture getHero(int i) {
        return hero[i];
    }

    public void setHero(Texture texture, int i) {
        this.hero[i] = texture;
    }

    public int getPositionX(int i) {
        return positionX[i];
    }

    public void setPositionX(int positionX, int i) {
        this.positionX[i] = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

}
package com.stavrosgatos.run.garbager.common.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.stavrosgatos.run.garbager.assets.Can;
import com.stavrosgatos.run.garbager.common.constants.Constants;

import java.util.Random;

/**
 * The class Utils.
 * Created by stike92 on 4/22/2018.
 * Contains ancillary methods.
 */
public class Utils {

    private int gravity;
    private int speed;
    private int score;
    private int heroState;
    private int touchedTimes;
    private int emptyCans;
    private int visibleEmptyCans;
    private boolean gameOver;
    private boolean gameJustStarted;

    private Random random = new Random();

    /**
     * @return the gravity
     */
    public int getGravity() {

        return gravity;
    }

    /**
     * @param gravity the gravity
     */
    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * Set the score increased by one.
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     *
     * @return the hero state
     */
    public int getHeroState() {
        return heroState;
    }

    public void setHeroState(int heroState) {
        this.heroState = heroState;
    }

    public int getTouchedTimes() {
        return touchedTimes;
    }

    public void setTouchedTimes(int touchedTimes) {
        this.touchedTimes = touchedTimes;
    }

    public int getEmptyCans() {
        return emptyCans;
    }

    public void setEmptyCans(int emptyCans) {
        this.emptyCans = emptyCans;
    }

    public int getVisibleEmptyCans() {
        return visibleEmptyCans;
    }

    public void setVisibleEmptyCans(int visibleEmptyCans) {
        this.visibleEmptyCans = visibleEmptyCans;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public boolean isGameJustStarted() {
        return gameJustStarted;
    }

    public void setGameJustStarted(boolean gameJustStarted) {
        this.gameJustStarted = gameJustStarted;
    }

    /**
     *
     * Get the center position x of a texture.
     * @param texture The texture
     * @return position x
     */
    public int getCenterPositionX (Texture texture) {
        return  Gdx.graphics.getWidth() / 2 - texture.getWidth() / 2;

    }

    /**
     * Get the speed.
     * @return the speed
     */
    public int getSpeed (int score) {
        return Constants.INITIAL_SPEED + ( (score + 1) / 25);
    }

    /**
     * Get the distance.
     * @return the distance
     */
    public int getDistance () {
        return  random.nextInt(Gdx.graphics.getWidth() / 2);
    }

    public int getNumberOfCans() {
        return  random.nextInt(Constants.CANS);
    }

    public int getNumberOfEmptyCans() {
        return  random.nextInt(Constants.EMPTY_CANS);
    }

    /**
     * Get a andom  can position y.
     * @return the can position y
     */
    public int getCanPositionY () {
        return Constants.JUMP_HEIGHT;
    }

    public void changeHeroState(int heroState) {
        if (heroState < 2) {
            this.heroState += 1;
        } else {
            this.heroState = 0;
        }
    }
}
